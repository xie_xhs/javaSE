package com.xhs;

import java.util.Arrays;

public class ArrayDemo3 {
    public static void main(String[] args) {
        int[] a= {1,5,2,4,11,3,0,1,44,9};
        System.out.println(Arrays.toString(bubbleSort(a)));
    }

    /**
     * 冒泡排序,降序
     * @param array int 类型数组
     * @return 返回已经排序好的数组
     */
    public static int[] bubbleSort(int[] array){
        //外层循环，判断需要走的次数
        for (int i = 0; i < array.length-1; i++) {
            //内层循环，比较两个数，如果第一个数比第二个数大，则交换位置
            for (int j = 0; j < array.length - 1 - i; j++) {
                if (array[j + 1] > array[j]) {
                    //交换两个值
                    int temp = array[j];
                    array[j] = array[j + 1];
                    array[j + 1] = temp;
                }
            }
        }
        return array;
    }
}
