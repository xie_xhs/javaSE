package com.xhs;

import java.util.Arrays;

public class ArrayDemo4 {
    public static void main(String[] args) {
        System.out.println("=======================");
        System.out.println("创建一个较为分布稀疏的二维数组");
        //构建一个二维数组
        int a = 11, b = 11;
        int[][] array = new int[a][b];
        array[1][2] = 1;
        array[2][3] = 2;

        //打印原二维数组
        for (int[] ints : array) {
            for (int anInt : ints) {
                System.out.print(anInt + "\t");
            }
            System.out.println();
        }
        System.out.println("=======================");
        System.out.println("稀疏数组");
        //计算原数组有效的元素个数（非0）
        int sum = 0;
        for (int[] ints : array) {
            for (int anInt : ints) {
                if (anInt != 0) {
                    sum++;
                }
            }
        }
        //构建一个稀疏数组,原数组有效（非0）元素作为行，3列的格式（数组第一行作为记录原数组的总行、总列、总有效个元素）
        int[][] array2 = new int[sum + 1][3];
        //填充表头
        array2[0][0] = a;
        array2[0][1] = b;
        array2[0][2] = sum;

        int count = 1;
        //循环遍历原数组，获取有效元素
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                if (array[i][j] != 0) {
                    array2[count][0] = i;//把原数组的行填入稀疏数组
                    array2[count][1] = j;//把原数组的列填入稀疏数组
                    array2[count][2] = array[i][j];//把原数组的值填入稀疏数组
                    count++;
                }
            }
        }
        //打印已经生成的稀疏数组
        for (int[] ints : array2) {
            for (int anInt : ints) {
                System.out.print(anInt + "\t");
            }
            System.out.println();
        }

        System.out.println("=======================");
        System.out.println("还原数组");
        //创建一个数组，读取稀疏数组的表头
        int[][] array3 = new int[array2[0][0]][array2[0][1]];
        //从第二行还原数组
        for (int i = 1; i < array2.length; i++) {
            array3[array2[i][0]][array2[i][1]] = array2[i][2];
        }


        //打印还原后的数组
        for (int[] ints : array3) {
            for (int anInt : ints) {
                System.out.print(anInt + "\t");
            }
            System.out.println();
        }
    }
}
