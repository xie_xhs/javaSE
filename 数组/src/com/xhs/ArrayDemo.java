package com.xhs;

public class ArrayDemo {
    public static void main(String[] args) {
        //定义数组,首选的方式
        int[] array;
        //定义数组，早期为了让方便c++人员使用，弄成一样的定义。
        int array2[];

        array =new int[10];//存放10个int类型的元素

        //直接赋值
        int[] array4 = new int[10];

        int[] array5 = {1,2,3,4};


//        for (int i = 0; i < array.length; i++) {
//            System.out.println(array[i]);
//
//        }

    }
}
