package com.xhs;

import java.util.Arrays;

public class ArrayDemo2 {
    public static void main(String[] args) {
        int[] a = {1, 12333, 6, 222, 4423414, 622111};
        //使用Arrays的静态方法打印数组元素
        System.out.println(Arrays.toString(a));
        //自己实现打印数组元素
        printArray(a);
        System.out.println();
        //打印出排序的数组元素,默认升序
        Arrays.sort(a);
        System.out.println(Arrays.toString(a));


    }

    /**
     * 打印数组元素
     * @param number int类型的数组
     */
    public static void printArray(int[] number) {
        for (int i = 0; i < number.length; i++) {
            if (i == 0)
                System.out.print("[");
            if (i == number.length - 1)
                System.out.print(number[i] + "]");
            else
                System.out.print(number[i] + ", ");
        }
    }
}
