package com.xhs;

import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * 服务端
 */
public class Server {
    public static void main(String[] args) throws IOException {
        ServerSocket ss = null;
        Socket socket = null;
        InputStream is = null;
        DataInputStream dis = null;
        try {
            //创建连接
            ss = new ServerSocket(7788);
            //等待连接
            socket = ss.accept();
            //读取发送过来的数据
            is = socket.getInputStream();
            dis = new DataInputStream(is);
            //展示数据
            System.out.println(dis.readUTF());

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            //关闭资源
            if (dis != null) {
                dis.close();
            }
            if (is != null) {
                is.close();
            }
            if (socket != null) {
                socket.close();
            }
            if (ss != null) {
                ss.close();
            }
        }
    }
}
