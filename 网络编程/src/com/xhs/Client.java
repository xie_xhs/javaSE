package com.xhs;

import java.io.*;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;

/**
 * 客户端
 */
public class Client {
    public static void main(String[] args) throws IOException {
        OutputStream os = null;
        Socket socket = null;
        DataOutputStream dos = null;
        try {
            InetAddress serverIP = InetAddress.getByName("127.0.0.1");
            int port = 7788;
            socket = new Socket(serverIP, port);
            os = socket.getOutputStream();
            dos = new DataOutputStream(os);

            dos.writeUTF(readFile("D:\\text.txt"));
        } catch (UnknownHostException e) {
            e.printStackTrace();
        } finally {
            if (dos != null)
                dos.close();

            if (os != null)
                os.close();

            if (socket != null)
                socket.close();
        }
    }
    /**
     * 读入TXT文件
     */
    public static String readFile(String pathname) {
        StringBuilder stringBuilder =stringBuilder = new StringBuilder();

        //防止文件建立或读取失败，用catch捕捉错误并打印，也可以throw;
        //不关闭文件会导致资源的泄露，读写文件都同理
        //Java7的try-with-resources可以优雅关闭文件，异常时自动关闭文件；详细解读https://stackoverflow.com/a/12665271
        try (FileReader reader = new FileReader(pathname);
             BufferedReader br = new BufferedReader(reader) // 建立一个对象，它把文件内容转成计算机能读懂的语言
        ) {
            String line;

            //网友推荐更加简洁的写法
            while ((line = br.readLine()) != null) {
                // 一次读入一行数据
//                System.out.println(line);
                stringBuilder.append(line);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
        return stringBuilder.toString();
    }
}
