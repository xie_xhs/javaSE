package interfaceProject;

public class Product {
	private String name;
	private double price;

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	public Product(String name, double price) {
		super();
		this.name = name;
		this.price = price;
	}
	public Product() {
		super();
	}

}



package interfaceProject;

public class Tourist extends Customer{

}

package interfaceProject;

import java.util.ArrayList;

public class CommonCustomer extends Customer implements IComputor{

	public double compute(Product p, double number) {
		return p.getPrice()*number*(1-0.1);
	}

	public double compute(ArrayList<OrderItem> ois) {
		// TODO Auto-generated method stub
		return 0;
	}

}

package interfaceProject;

import java.util.ArrayList;

public class PlusCustomer extends Customer implements IComputor {

	public double compute(Product p, double number) {
		return p.getPrice()*number*(1-0.15);
	}

	public double compute(ArrayList<OrderItem> ois) {
		// TODO Auto-generated method stub
		return 0;
	}

}


package interfaceProject;

public class Test {

	public static double compute(IComputor ic,Product p,double number)
	{
		return ic.compute(p, number);
	}
	
	public static void main(String[] args) {
		
		Product p=new Product("phone",3599.99);
		
		CommonCustomer cc=new CommonCustomer();
		cc.setName("Zhangsan");
		
		System.out.println(cc.compute(p, 10));;
		
		System.out.println(compute(cc,p,10));
		
		PlusCustomer pc=new PlusCustomer();
		pc.setName("Lisi");
		System.out.println(pc.compute(p, 10));
		System.out.println(compute(pc,p,10));

	}

}












