定义书本类
author
bookname
price


定义书本类数组3~5，保存3~5对象，并按价格排序

按书名查找书本数组中的对象。如果有输出，没有输出“找不到”

[0   1   2    3    4     5]
 5   6   4    1    10   8
 5   4   1    6    8   10


[0   1   2    3    4]     5
 4   1   5    6    8     10

[0   1   2    3  ]  4     5
 4   1   5    6    8     10



package arrayObjectProject;

public class Book {
	String author;
	String name;
	double price;
}


package arrayObjectProject;

import java.util.Scanner;

public class Test {

	public static void main(String[] args) {
		
		Book[] books;
		books=new Book[3];
		Scanner sc=new Scanner(System.in);
		
		for(int i=0;i<books.length;i++)
		{
			books[i]=new Book();
			books[i].author=sc.next();
			books[i].name=sc.next();
			books[i].price=sc.nextDouble();//20.5+50*Math.random();
		}
		
		//排序
		for(int i=0;i<books.length-1;i++)
		{
			for(int k=0;k<books.length-1-i;k++)
			{
				if(books[k].price>books[k+1].price)
				{
					Book temp;
					temp=books[k];
					books[k]=books[k+1];
					books[k+1]=temp;
				}
			}
		}
		
		String searchStr;
		
		searchStr=sc.next();
		
		boolean flag;
		flag=false;
		
		for(int i=0;i<books.length;i++)
		{
			if(searchStr.equals(books[i].name))
			{
				flag=true;
				System.out.println(books[i].author+"\t"+books[i].name+"\t"+books[i].price);
			}
		}
		if(!flag)
		{
			System.out.println("no found!");
		}
		
System.out.println();
	}

}
