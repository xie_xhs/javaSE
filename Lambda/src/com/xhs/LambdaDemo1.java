package com.xhs;

public class LambdaDemo1 {
    public static void main(String[] args) {
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                System.out.println("1111");
            }
        };
        runnable.run();
        //Lambda表达式
        Runnable runnable2 = () -> System.out.println("1111");
        runnable2.run();
    }
}
