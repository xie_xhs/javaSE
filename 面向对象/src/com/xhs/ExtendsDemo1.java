package com.xhs;

public class ExtendsDemo1 {
    public static void main(String[] args) {
        new A();
    }
}
class A{
    public A(){
        System.out.println("A无参构造器！");
    }
    {
        System.out.println("A普通代码块！");
    }
    static{
        System.out.println("A静态代码块！");
    }

}
class B extends A{
    {
        System.out.println("B普通代码块！");
    }
    static{
        System.out.println("B静态代码块！");
    }
    public B(){
        System.out.println("B无参构造器！");
    }
}